import styled from 'styled-components'

export const BlockHeadingWrapper = styled.div`
  display: block;
  width: 100%;
  color: white;
  font-size: 14px;
  padding: 8px 15px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  .fa {
    margin-right: 7px;
  }
`