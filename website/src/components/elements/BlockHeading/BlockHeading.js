import React from 'react'
import { BlockHeadingWrapper } from './styled-components'
import PropTypes from 'prop-types'

const BlockHeading = ({ children, theme }) => {
  return (
    <BlockHeadingWrapper
      className={theme === 'green' ? 'green-gradient' : 'orange-gradient'}>
      {children}
    </BlockHeadingWrapper>
  )
}

BlockHeading.propTypes = {
  theme: PropTypes.oneOf(['green', 'orange']),
}

export default BlockHeading