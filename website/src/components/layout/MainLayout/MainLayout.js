import React from 'react'
import PropTypes from 'prop-types'
import { LayoutWrapper } from './styled-components'
import MainHeader from '../../organisms/MainHeader'
import MainFooter from '../../organisms/MainFooter'
import LoadingSpinner from '../../organisms/LoadingSpinner'
import ScrollUpButton from 'react-scroll-up-button'

const MainLayout = props => {

  const { children } = props

  return (
    <LayoutWrapper>
      <MainHeader/>
      {children}
      <MainFooter/>
      <LoadingSpinner/>
      <ScrollUpButton
        ContainerClassName="ScrollUpButton__Container"
        TransitionClassName="ScrollUpButton__Toggled"
      >
        <span/>
      </ScrollUpButton>
    </LayoutWrapper>
  )
}

MainLayout.propTypes = {
  children: PropTypes.node,
}

export default MainLayout