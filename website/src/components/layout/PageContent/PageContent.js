import React from 'react'
import {
  PageContentWrapper, MainContent,
} from './styled-components'
import { Container } from '../Container/Container'

const PageContent = props => {

  const { children } = props

  return (
    <PageContentWrapper>
      <Container>
        <MainContent>
          {children}
        </MainContent>
      </Container>
    </PageContentWrapper>
  )

}

export default PageContent