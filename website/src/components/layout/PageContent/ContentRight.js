import React from 'react'
import {
  ContentRightWrapper,
} from './styled-components'

const ContentRight = ({ children }) => {
  return (
    <ContentRightWrapper>
      {children}
    </ContentRightWrapper>
  )
}

export default ContentRight