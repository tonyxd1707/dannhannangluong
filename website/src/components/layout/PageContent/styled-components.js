import styled from 'styled-components'

export const PageContentWrapper = styled.div`
  padding: 10px;
  background-color: #517f41;
`
export const MainContent = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 10px;
  border-radius: 10px;
  box-shadow: 0 0 10px rgba(0,0,0,.5);
  background-color: #fff;
`
export const ContentLeftWrapper = styled.div`
  width: 68%;
`
export const ContentRightWrapper = styled.div`
  width: 31%;
  margin-left: auto;
`