import React from 'react'
import {
  ContentLeftWrapper,
} from './styled-components'

const ContentLeft = ({ children }) => {
  return (
    <ContentLeftWrapper>
      {children}
    </ContentLeftWrapper>
  )
}

export default ContentLeft