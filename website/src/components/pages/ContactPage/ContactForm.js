import React from 'react'
import {
  Form, Button, Input,
} from 'antd'
import validator from '../../../validator'
import axios from 'axios'
import { apiUrl } from '../../../config'
import { inject, observer } from 'mobx-react'
import { message } from 'antd'

const { TextArea } = Input

const ContactForm = ({ loadingAnimationStore }) => {

  const CustomForm = props => {

    const { getFieldDecorator } = props.form

    const handleSubmit = e => {
      e.preventDefault()
      props.form.validateFields((err, values) => {
        if (!err) {
          const querySetting = {
            method: 'post',
            url: `${apiUrl}/messages`,
            data: values,
          }
          loadingAnimationStore.showSpinner(true)
          axios(querySetting)
            .then(() => {
              message.success('Tin nhắn của bạn đã được gửi đi')
              loadingAnimationStore.showSpinner(false)
              props.form.resetFields()
            })
            .catch(error => {
              console.log(error)
              message.error('Có lỗi xảy ra, xin vui lòng thử lại')
              loadingAnimationStore.showSpinner(false)
            })
        }
      })
    }

    return (
      <Form onSubmit={handleSubmit}>
        <Form.Item label={'Họ và tên'}>
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Vui lòng nhập đầy đủ họ và tên',
              },
              {
                validator: validator.validateEmptyString,
              },
            ],
          })(
            <Input placeholder={'Họ và tên'}/>,
          )}
        </Form.Item>
        <Form.Item label={'Số điện thoại'}>
          {getFieldDecorator('phone', {
            rules: [
              {
                validator: validator.validatePhoneNumber,
              },
            ],
          })(
            <Input placeholder={'Nhập số điện thoại của bạn'}/>,
          )}
        </Form.Item>
        <Form.Item label={'Địa chỉ email'}>
          {getFieldDecorator('email', {
            rules: [
              {
                validator: validator.validateEmail,
              },
            ],
          })(
            <Input placeholder={'Địa chỉ email'}/>,
          )}
        </Form.Item>
        <Form.Item label={'Tin nhắn'}>
          {getFieldDecorator('comment', {
            rules: [
              {
                validator: validator.validateEmptyString,
              },
            ],
          })(
            <TextArea rows={'4'} placeholder={'Lời nhắn'}/>,
          )}
        </Form.Item>
        <Button type={'primary'} htmlType={'submit'}>
          Gửi thông tin liên hệ
        </Button>
      </Form>
    )

  }

  const WrappedCustomForm = Form.create({ name: 'contact-form' })(CustomForm)

  return <WrappedCustomForm/>
}

export default inject('loadingAnimationStore')(observer(ContactForm))