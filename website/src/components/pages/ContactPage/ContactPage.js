import React, { Fragment, useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import MainLayout from '../../layout/MainLayout'
import PageContent from '../../layout/PageContent'
import axios from 'axios'
import { apiUrl } from '../../../config'
import Markdown from 'markdown-to-jsx'
import {
  ContactPageWrapper, ContactFormWrapper,
} from './styled-components'
import { inject, observer } from 'mobx-react'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import {
  Row, Col,
} from 'antd'
import ContactForm from './ContactForm'

const ContactPage = props => {

  const { commonStore } = props

  const [contact, setContact] = useState(null)

  useEffect(() => {
    axios({
      method: 'get',
      url: `${apiUrl}/contacts/1`,
    }).then(response => {
      setContact(response.data)
    }).catch(error => console.log(error))
  }, [])

  useEffect(() => {
    commonStore.setPage('Liên Hệ')
  }, [])

  return (
    <Fragment>
      <Helmet>
        <title>Liên hệ</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}/>
        <PageContent>
          <ContactPageWrapper>
            {
              contact && contact.maplink
                ? <div
                  style={{
                    width: '100%',
                    marginBottom: 10,
                  }}
                  dangerouslySetInnerHTML={{ __html: contact.maplink }}/>
                : null
            }
            <Row type={'flex'} justify={'space-between'}>
              <Col xs={24} md={12}>
                <ContactFormWrapper>
                  <ContactForm/>
                </ContactFormWrapper>
              </Col>
              <Col xs={24} md={11}>
                {
                  contact && contact.description
                    ? <Markdown>{contact.description}</Markdown>
                    : null
                }
              </Col>
            </Row>
          </ContactPageWrapper>
        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default inject('commonStore')(observer(ContactPage))
