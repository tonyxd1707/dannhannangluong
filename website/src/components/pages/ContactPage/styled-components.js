import styled from 'styled-components'

export const ContactPageWrapper = styled.div`
  padding: 30px;
  display: block;
  width: 100%;
  h2 {
    color: #376530;
    text-transform: uppercase;
    font-size: 16px;
    margin-bottom: 0;
    margin-top: 15px;
  }
  p {
    font-size: 14px;
    margin-bottom: 0;
  }
`
export const ContactFormWrapper = styled.div`
  .ant-form-item {
    margin-bottom: 10px;
    .ant-form-item-label {
      line-height: 1.5;
    }
    textarea.ant-input {
      margin-top: 5px;
    }
  }
  .ant-btn-primary {
    margin-top: 15px;
    background-color: #517f41 !important;
  }
`