import React, { Fragment, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import MainLayout from '../../layout/MainLayout'
import PageContent from '../../layout/PageContent'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import { inject, observer } from 'mobx-react'
import commonStore from '../../../stores/commonStore'

const PartnersPage = () => {

  useEffect(() => {
    commonStore.setPage('Đối Tác')
  }, [])

  return (
    <Fragment>
      <Helmet>
        <title>Đối tác</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}/>
        <PageContent>
          Content here
        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default inject('commonStore')(observer(PartnersPage))
