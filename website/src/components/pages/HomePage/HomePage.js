import React, { useEffect, Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { Helmet } from 'react-helmet'
import MainLayout from '../../layout/MainLayout'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import PageContent from '../../layout/PageContent'
import ContentRight from '../../layout/PageContent/ContentRight'
import LiveSupport from '../../organisms/LiveSupport'
import VideoPanel from '../../organisms/VideoPanel'
import PartnersPanel from '../../organisms/PartnersPanel'
import NoticeAndDocumentPanel from '../../organisms/NoticeAndDocumentPanel'
import FacilitiesListPanel from '../../organisms/FacilitiesListPanel'
import ContentLeft from '../../layout/PageContent/ContentLeft'
import StandardsPanel from '../../organisms/StandardsPanel'
import FAQPanel from '../../organisms/FAQPanel'
import ArticlesPanel from '../../organisms/ArticlesPanel'
import HotNewArticlePanel from '../../organisms/HotNewArticlePanel'

const HomePage = props => {

  const {
    documentsStore, facilitiesStore,
    loadingAnimationStore, questionsAnswersStore,
    standardsStore, articlesStore,
    partnersStore,
  } = props

  const Divider = () =>
    <div style={{
      display: 'block',
      width: '100%',
      height: 10,
    }}/>

  useEffect(() => {
    loadingAnimationStore.showSpinner(true)
    let p1 = documentsStore.getAllDocuments()
    let p2 = facilitiesStore.getAllFacilities()
    let p3 = standardsStore.getAllStandards()
    let p4 = questionsAnswersStore.getAllQuestions()
    let p5 = articlesStore.getAllArticles(`?_sort=updated_at:desc`)
    let p6 = partnersStore.getAllPartners()
    Promise.all([p1, p2, p3, p4, p5, p6])
      .then(() => {
        loadingAnimationStore.showSpinner(false)
      })
  }, [])

  return (
    <Fragment>
      <Helmet>
        <title>Trang Chủ</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow
          text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}
        />
        <PageContent>

          <ContentLeft>
            <HotNewArticlePanel/>
            <Divider/>
            <ArticlesPanel category={''}/>
            <Divider/>
            <StandardsPanel/>
            <Divider/>
            <FAQPanel isWidget={true}/>
          </ContentLeft>

          <ContentRight>
            <LiveSupport/>
            <Divider/>
            <VideoPanel/>
            <Divider/>
            <PartnersPanel/>
            <Divider/>
            <NoticeAndDocumentPanel/>
            <Divider/>
            <FacilitiesListPanel/>
          </ContentRight>

        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default inject(
  'documentsStore', 'facilitiesStore',
  'loadingAnimationStore', 'standardsStore',
  'questionsAnswersStore', 'articlesStore',
  'partnersStore',
)(observer(HomePage))