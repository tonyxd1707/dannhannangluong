import React, { Fragment } from 'react'
import { withRouter, Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import BlockHeading from '../../elements/BlockHeading'
import {
  CategoriesPanelWrapper,
} from './styled-components'

const CategoriesPanel = props => {

  const {
    history,
    commonStore, articlesStore,
  } = props

  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-folder-open" aria-hidden="true"/>
        Danh mục
      </BlockHeading>
      <CategoriesPanelWrapper>
        <li>Danh sách cơ sở thử nghiệm</li>
        <li>Danh mục tiêu chuẩn</li>
        <li>Văn bản & thông báo</li>
      </CategoriesPanelWrapper>
    </Fragment>
  )
}

export default withRouter(inject(
  'commonStore', 'articlesStore',
)(CategoriesPanel))