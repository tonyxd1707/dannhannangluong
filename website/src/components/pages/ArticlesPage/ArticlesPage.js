import React, { Fragment, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import { inject, observer } from 'mobx-react'
import { withRouter, Link } from 'react-router-dom'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import MainLayout from '../../layout/MainLayout'
import PageContent from '../../layout/PageContent'
import ContentLeft from '../../layout/PageContent/ContentLeft'
import ContentRight from '../../layout/PageContent/ContentRight'
import CategoriesPanel from './CategoriesPanel'
import ArticlePanel from '../../organisms/ArticlesPanel'

const ArticlesPage = props => {

  const {
    history, match,
    commonStore, articlesStore,
    facilitiesStore, documentsStore,
    loadingAnimationStore,
  } = props

  const Divider = () =>
    <div style={{
      display: 'block',
      height: 10,
    }}/>

  useEffect(() => {
    commonStore.setPage('Dịch Vụ Luật Sư')
  }, [])

  useEffect(() => {
    loadingAnimationStore.showSpinner(true)
    Promise.all([
      documentsStore.getAllDocuments(),
      facilitiesStore.getAllFacilities(),
      articlesStore.getAllArticles(`?_sort=updated_at:desc`),
    ]).then(() => {
      loadingAnimationStore.showSpinner(false)
    })
  }, [])

  return (
    <Fragment>
      <Helmet>
        <title>Bài viết</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}/>
        <PageContent>

          <ContentLeft>
            <ArticlePanel category={'Danh sách cơ sở thử nghiệm'}/>
            <Divider/>
            <ArticlePanel category={'Danh mục tiêu chuẩn'}/>
          </ContentLeft>

          <ContentRight>
            <CategoriesPanel/>
          </ContentRight>

        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default withRouter(inject(
  'commonStore', 'articlesStore',
  'documentsStore', 'facilitiesStore',
  'loadingAnimationStore',
)(observer(ArticlesPage)))
