import React, { Fragment, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import MainLayout from '../../layout/MainLayout'
import PageContent from '../../layout/PageContent'
import { inject, observer } from 'mobx-react'
import { withRouter, Link } from 'react-router-dom'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import FAQPanel from '../../organisms/FAQPanel'

const QAPage = props => {

  const {
    questionsAnswersStore, commonStore,
    loadingAnimationStore,
  } = props

  useEffect(() => {
    commonStore.setPage('Hỏi Đáp')
    window.scrollTo(0, 0)
  }, [])

  useEffect(() => {
    loadingAnimationStore.showSpinner(true)
    questionsAnswersStore.getAllQuestions()
      .then(() => loadingAnimationStore.showSpinner(false))
  }, [])

  return (
    <Fragment>
      <Helmet>
        <title>Hỏi đáp</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}/>
        <PageContent>
          <FAQPanel isWidget={false}/>
        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default withRouter(inject(
  'commonStore', 'questionsAnswersStore',
  'loadingAnimationStore',
)(observer(QAPage)))
