import styled from 'styled-components'

export const ArticleWrapper = styled.article`
  padding: 30px;
  h1 {
    text-transform: uppercase;
    font-size: 20px;
  }
  img {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
`