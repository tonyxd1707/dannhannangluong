import React, { Fragment, useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import MainLayout from '../../layout/MainLayout'
import WelcomeBandRow from '../../organisms/WelcomeBandRow'
import PageContent from '../../layout/PageContent'
import axios from 'axios'
import { apiUrl } from '../../../config'
import Markdown from 'markdown-to-jsx'
import { ArticleWrapper } from './styled-components'
import { inject, observer } from 'mobx-react'

const IntroPage = props => {

  const { commonStore } = props

  useEffect(() => {
    axios({
      method: 'get',
      url: `${apiUrl}/intros/1`,
    }).then(response => {
      setContent(response.data)
    }).catch(error => console.log(error))
  }, [])

  useEffect(() => {
    commonStore.setPage('Giới Thiệu')
  }, [])

  const [content, setContent] = useState(null)

  return (
    <Fragment>
      <Helmet>
        <title>Giới thiệu</title>
      </Helmet>
      <MainLayout>
        <WelcomeBandRow
          text={'Chào mừng đến với dán nhãn năng lượng dannhannangluong.com'}/>
        <PageContent>
          <ArticleWrapper>
            {
              content
                ? <Markdown>{content.Content}</Markdown>
                : null
            }
          </ArticleWrapper>
        </PageContent>
      </MainLayout>
    </Fragment>
  )
}

export default inject('commonStore')(observer(IntroPage))
