import styled from 'styled-components'

export const QAListWrapper = styled.ul`
  margin-top: ${props => props.isWidget ? '10px' : '20px'};
  padding: ${props => props.isWidget ? '0' : '0 15px 0'};
  li {
    .heading {
      font-weight: normal;
      color: #333333;
      margin-bottom: 5px;
      strong {
        color: #fc0000;
      }
    }
    .content {
      border: 1px solid #e1e1e1;
      background-color: #edfbe1;
      padding: 55px 15px 15px;
      margin-bottom: 20px;
      position: relative;
      &:before {
        display: block;
        content: 'Trả lời';
        color: white;
        font-size: 14px;
        position: absolute;
        top: 15px;
        left: 15px;
        background-color: #517f41;
        padding: 5px 10px;
      }
    }
  }
`
export const Navigation = styled.div`
  text-align: right;
  color: #517f3f;
  margin-top: -10px;
  margin-bottom: 10px;
  &:hover {
    cursor: pointer;
  }
  .fa {
    margin-left: 5px;
  }
`