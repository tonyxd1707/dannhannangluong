import React, { Fragment, useEffect } from 'react'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import BlockHeading from '../../elements/BlockHeading'
import {
  QAListWrapper, Navigation,
} from './styled-components'
import Markdown from 'markdown-to-jsx'

const FAQPanel = props => {

  const {
    isWidget,
    history,
    questionsAnswersStore, commonStore,
  } = props

  const QAItem = props => {
    const { id, question, answer } = props.item
    return (
      <li>
        <h2 className={'heading'}>
          <strong>Câu hỏi {id}:</strong> {question}
        </h2>
        <div className={'content'}>
          <Markdown>
            {answer}
          </Markdown>
        </div>
      </li>
    )
  }

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  return (
    <Fragment>
      {
        isWidget
          ? (
            <BlockHeading theme={'orange'}>
              <strong>HỎI ĐÁP</strong>
            </BlockHeading>
          ) : null
      }
      <QAListWrapper isWidget={isWidget}>
        {
          questionsAnswersStore.qaList.length === 0
            ? null :
            isWidget
              ? questionsAnswersStore.qaList
                .slice(0, 3)
                .map(item =>
                  <QAItem key={item.id} item={item}/>,
                )
              : questionsAnswersStore.qaList
                .map(item =>
                  <QAItem key={item.id} item={item}/>,
                )
        }
      </QAListWrapper>
      {
        isWidget
          ? (
            <Navigation onClick={() => handleNavigation('/hoi-dap', 'Hỏi Đáp')}>
              Xem thêm
              <i className="fa fa-angle-double-right" aria-hidden="true"/>
            </Navigation>
          )
          : null
      }
    </Fragment>
  )
}

export default withRouter(inject(
  'commonStore', 'questionsAnswersStore',
)(observer(FAQPanel)))