import React, { Fragment } from 'react'
import { VideoWrapper } from './styled-components'
import BlockHeading from '../../elements/BlockHeading'

const VideoPanel = () => {
  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-floppy-o" aria-hidden="true"/>
        Video Clip Nổi Bật
      </BlockHeading>
      <VideoWrapper>
        <iframe
          width="1280" height="720"
          src="https://www.youtube.com/embed/hHke69Rvibs"
          frameBorder="0" allowFullScreen
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"/>
      </VideoWrapper>
    </Fragment>
  )
}

export default VideoPanel