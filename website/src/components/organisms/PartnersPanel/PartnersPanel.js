import React, { Fragment, useEffect } from 'react'
import BlockHeading from '../../elements/BlockHeading'
import {
  PartnersPanelWrapper, PartnerItem,
} from './styled-components'
import { inject, observer } from 'mobx-react'
import { toJS } from 'mobx'
import { withRouter, Link } from 'react-router-dom'
import { apiUrl } from '../../../config'
// Slick
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'

const PartnersPanel = props => {

  const {
    partnersStore,
  } = props

  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: false,
  }

  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-globe" aria-hidden="true"/>
        Đối tác sử dụng dịch vụ
      </BlockHeading>
      <PartnersPanelWrapper>
        <Slider {...settings}>
          {
            partnersStore.partnersList.map(item =>
              <PartnerItem
                itemName={item.name}
                bgItem={
                  item.thumbnail
                  && `${apiUrl}${item.thumbnail.url}`
                }
                key={item.id}/>,
            )
          }
        </Slider>
      </PartnersPanelWrapper>
    </Fragment>
  )
}

export default withRouter(inject(
  'partnersStore',
)(observer(PartnersPanel)))