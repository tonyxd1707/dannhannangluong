import styled, { css } from 'styled-components'

export const PartnersPanelWrapper = styled.div`
  background-color: #f2f2f2;
  padding: 15px;
  font-size: 14px;
  .slick-arrow {
    &:before {
      color: #517f3f;
    }
  }
`

export const PartnerItem = styled.div`
  height: 130px;
  position: relative;
  padding: 5px;
  ${props => props.bgItem && css`
    &:after {
      display: block;
      content: '';
      background: url(${props.bgItem}) white no-repeat center center;
      background-size: 100% auto;
      width: calc(100% - 10px);
      height: calc(100% - 10px);
      border: 1px solid #c2c2c2;
      position: absolute;
      top: 5px;
      left: 5px;
    }
  `}
  ${props => props.itemName && css`
    &:before {
      display: block;
      content: '${props.itemName}';
      background-color: #517f3f;
      color: white;
      font-size: 13px;
      padding: 5px 10px;
      position: absolute;
      bottom: 5px;
      left: 5px;
      width: calc(100% - 10px);
      z-index: 1;
    }
  `}
`