import React, { Fragment } from 'react'
import BlockHeading from '../../elements/BlockHeading'
import { Table } from 'antd'
import { withRouter, Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'

const StandardsPanel = props => {

  const {
    history,
    standardsStore, commonStore,
  } = props

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  const tableColumns = [
    {
      title: () => (
        <Fragment>
          Số<br/>
          TT
        </Fragment>
      ),
      key: 'position',
      render: record => record.id,
      width: '10%',
    },
    {
      title: 'Tên sản phẩm',
      key: 'name',
      render: record => record.name,
      width: '25%',
    },
    {
      title: 'TCVN',
      key: 'tcvn',
      render: record => record.tcvn,
      width: '15%',
    },
    {
      title: 'Tên tiêu chuẩn',
      key: 'standard',
      render: record => record.description,
      width: '50%',
    },
  ]

  return (
    <Fragment>
      <BlockHeading theme={'orange'}>
        <strong>DANH MỤC TIÊU CHUẨN</strong>
      </BlockHeading>
      <Table
        bordered rowKey={record => record.id} columns={tableColumns}
        dataSource={standardsStore.standardsList}
        pagination={{
          hideOnSinglePage: true,
        }}
      />
    </Fragment>
  )
}

export default withRouter(inject(
  'commonStore', 'standardsStore',
)(observer(StandardsPanel)))