import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withRouter, Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import BlockHeading from '../../elements/BlockHeading'
import Markdown from 'markdown-to-jsx'
import moment from 'moment'
import {
  ArticleWrapper, NormalArticleLink, NewestArticleContainer,
} from './styled-components'

const ArticlesPanel = props => {

  const {
    history, category,
    commonStore, articlesStore,
    documentsStore, facilitiesStore,
  } = props

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  const ArticleItem = props => {

    const { item, index } = props

    if (index === 0) {
      return (
        <li>
          <NewestArticleContainer>
            <div className="cover">
              <img src={`${process.env.PUBLIC_URL}/article-dummy-cover.png`} alt=""/>
            </div>
            <div className="content">
              <h3>
                {item.title}
              </h3>
              <time>
                {moment(item.updated_at).format('DD-MM-YYYY')}
              </time>
              <Markdown>
                {
                  item.content.length > 300
                    ? item.content.substring(0, 300).concat('...')
                    : item.content
                }
              </Markdown>
              {
                category
                  ? null : (
                    <div className="cta">
                      Xem thêm
                      <i className="fa fa-angle-double-right" aria-hidden="true"/>
                    </div>
                  )
              }
            </div>
          </NewestArticleContainer>
        </li>
      )
    }
    return (
      <li>
        <NormalArticleLink to={'/'} onClick={() => handleNavigation('/', 'Trang Chủ')}>
          <span>{item.title}</span>
          <time>{moment(item.updated_at).format('DD-MM-YYYY')}</time>
        </NormalArticleLink>
      </li>
    )
  }

  const renderBlockHeading = category => {
    switch (category) {
      case '':
        return <strong>DỊCH VỤ XIN DÁN NHÃN NĂNG LƯỢNG</strong>
      default:
        return <strong>{category.toUpperCase()}</strong>
    }
  }

  return (
    <Fragment>
      <BlockHeading theme={'orange'}>
        {renderBlockHeading(category)}
      </BlockHeading>
      <ArticleWrapper>
        {
          articlesStore.articlesList.length === 0
            ? null :
            articlesStore.articlesList
              .slice(0, 6)
              .map((item, index) =>
                <ArticleItem
                  index={index}
                  key={index}
                  item={item}
                />,
              )
        }
      </ArticleWrapper>
    </Fragment>
  )

}

ArticlesPanel.propTypes = {
  category: PropTypes.string.isRequired,
}

export default withRouter(inject(
  'commonStore', 'articlesStore',
  'documentsStore', 'facilitiesStore',
)(observer(ArticlesPanel)))