import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const ArticleWrapper = styled.ul`
  padding: 10px 10px 5px;
  background-color: #fff;
  border: 1px solid #e1e1e1;
  border-top: none;
  margin-bottom: 0;
`

export const NormalArticleLink = styled(Link)`
  position: relative;
  display: flex;
  justify-content: space-between;
  padding-left: 12px;
  margin-bottom: 5px;
  padding-right: 85px;
  &:before {
    display: block;
    content: '';
    width: 6px;
    height: 6px;
    background-color: #517f3f;
    position: absolute;
    top: 8px;
    left: 0;
  }
  span {
    color: #666;
  }
  time {
    color: #517f3f;
    position: absolute;
    top: 0;
    right: 0;
  }
`
export const NewestArticleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  flex-wrap: wrap;
  .cover {
    width: 50%;
    background-color: #f2f2f2;
    padding: 15px;
    border: 1px dashed #e1e1e1;
    border-radius: 10px;
    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
  .content {
    width: 48%;
    h3 {
      color: #517f3f;
      text-transform: uppercase;
      font-size: 16px;
      margin-bottom: 5px;
    }
    time {
      display: block;
      font-size: 12px;
      color: #517f3f;    
      margin-bottom: 15px;  
    }
    .cta {
      border-top: 1px dashed #cdcdcd;
      text-align: right;
      padding-top: 5px;
      color: #517f3f;
      .fa {
        margin-left: 5px;
      }
    }
  }
  @media screen and (max-width: 768px) {
    .cover, .content {
      width: 100%;
    }
    .cover {
      margin-bottom: 10px;
    }
  }
`