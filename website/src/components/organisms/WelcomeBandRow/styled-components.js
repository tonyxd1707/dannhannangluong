import styled from 'styled-components'

export const Wrapper = styled.div`
  background-color: #ffb200;
  padding: 5px 0;
  span {
    color: #517f41;
    text-transform: uppercase;
    font-size: 14px;
    
  } 
`