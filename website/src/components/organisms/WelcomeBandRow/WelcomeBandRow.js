import React from 'react'
import PropTypes from 'prop-types'
import { Wrapper } from './styled-components'
import { Container } from '../../layout/Container/Container'

const WelcomeBandRow = ({ text }) => {
  return (
    <Wrapper>
      <Container>
        <span>{text}</span>
      </Container>
    </Wrapper>
  )
}

WelcomeBandRow.propTypes = {
  text: PropTypes.string,
}

export default WelcomeBandRow