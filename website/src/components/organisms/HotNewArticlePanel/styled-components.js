import styled, { css } from 'styled-components'

export const TabWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
`

export const TabBar = styled.ul`
  width: 36%;
  display: flex;
  flex-direction: column;
  border-left: 1px solid white;
  position: relative;
  z-index: 1;
`

export const TabBarItem = styled.li`
  padding: 10px;
  h2 {
      font-size: 16px;      
      margin-bottom: 0;
    }
    time {
      font-size: 12px;
    }
  ${props => props.active && css`
    background-color: #557446;
    position: relative;
    &:before {
      display: block;
      content: '';
      width: 0;
      height: 0;
      border: 15px solid transparent;
      border-right: 15px solid #557446;
      position: absolute;
      top: 50%;
      right: 100%;
      transform: translateY(-50%);
    }
    * {
      color: white;
    }
  `}
`

export const TabContent = styled.div`
  width: 64%;
  position: relative;
  ${props => props.bgURL && css`
    background: url(${props.bgURL}) no-repeat center center;
    background-size: cover;
  `}
`

export const TabContentWrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: rgba(0,0,0,.5);
  padding: 10px;
  width: 100%;
  * {
    color: white;
  }
  h2 {
    font-size: 16px;
    margin-bottom: 5px;
  }
  p {
    font-size: 12px;
    margin-bottom: 0;
  }
`