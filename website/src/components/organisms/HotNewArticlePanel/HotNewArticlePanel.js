import React, { useState } from 'react'
import { inject, observer } from 'mobx-react'
import { withRouter, Link } from 'react-router-dom'
import { toJS } from 'mobx'
import moment from 'moment'
import {
  TabWrapper, TabBar, TabBarItem, TabContent, TabContentWrapper,
} from './styled-components'
import { apiUrl } from '../../../config'

const HotNewArticlePanel = props => {

  const {
    history,
    commonStore, articlesStore,
  } = props

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  const [selectedArticle, setSelectedArticle] = useState(0)

  const filteredHotArticle = toJS(articlesStore.articlesList)
    .filter(item => item.showOnHomePage)
    .slice(0, 3)

  const handleChangeTab = index => {
    setSelectedArticle(index)
  }

  return (
    <TabWrapper>
      <TabContent bgURL={
        filteredHotArticle[selectedArticle]
        && filteredHotArticle[selectedArticle].cover
        && `${apiUrl}${filteredHotArticle[selectedArticle].cover.url}`}>
        <TabContentWrapper>
          <h2>
            {
              filteredHotArticle[selectedArticle]
              && filteredHotArticle[selectedArticle].title
            }
          </h2>
          <p>
            {
              filteredHotArticle[selectedArticle]
              && filteredHotArticle[selectedArticle].excerpt
              && filteredHotArticle[selectedArticle].excerpt.substring(0, 100).concat('...')
            }
          </p>
        </TabContentWrapper>
      </TabContent>
      <TabBar>
        {
          filteredHotArticle.map((item, index) =>
            <TabBarItem
              onClick={() => handleChangeTab(index)}
              active={index === selectedArticle}
              key={item.id}>
              <h2>{item.title}</h2>
              <time>{moment(item.updated_at).format('DD-MM-YYYY')}</time>
            </TabBarItem>,
          )
        }
      </TabBar>
    </TabWrapper>
  )
}

export default withRouter(inject(
  'commonStore', 'articlesStore',
)(observer(HotNewArticlePanel)))