import React, { Fragment } from 'react'
import BlockHeading from '../../elements/BlockHeading'
import {
  LiveSupportContent
} from './styled-components'

const LiveSupport = () => {
  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-phone" aria-hidden="true"/>
        Hỗ trợ trực tuyến
      </BlockHeading>
      <LiveSupportContent>
        <i className="fa fa-phone" aria-hidden="true"/>
        <div className="name">
          Luật Sư: Hoàng Thị Nghĩa
        </div>
        <a href="tel:0987648889" className={'tel'}>
          0997-64-8889
        </a>
      </LiveSupportContent>
    </Fragment>
  )
}

export default LiveSupport