import styled from 'styled-components'

export const LiveSupportContent = styled.div`
  border: 1px dashed #c2c2c2;
  border-top: none;
  background-color: #f2f2f2;
  font-size: 14px;
  padding: 15px 15px 15px 60px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  position: relative;
  .name {
    color: #444444;
    font-size: 16px;
    font-weight: 500;
    margin-bottom: 0;
  }
  .tel {
    color: #fc0000;
    font-size: 20px;
  }
  .fa {
    position: absolute;
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
    font-size: 36px;
    color: #fc0000;
  }
`