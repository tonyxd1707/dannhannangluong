import React, { Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { withRouter, Link } from 'react-router-dom'
import BlockHeading from '../../elements/BlockHeading'
import { ListWrapper } from '../NoticeAndDocumentPanel/styled-components'

const FacilitiesListPanel = props => {

  const {
    history,
    facilitiesStore, commonStore,
  } = props

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-file-text" aria-hidden="true"/>
        Danh sách cơ sở thử nghiệm
      </BlockHeading>
      <ListWrapper>
        {
          facilitiesStore.facilitiesList.length > 0
            ? (
              facilitiesStore.facilitiesList
                .slice(0, 5)
                .map(item =>
                  <li key={item.id}>
                    {item.name}
                  </li>,
                )
            ) : null
        }
        <li>
          Xem thêm
          <i className="fa fa-angle-double-right" aria-hidden="true"/>
        </li>
      </ListWrapper>
    </Fragment>
  )
}

export default withRouter(inject(
  'facilitiesStore', 'commonStore',
)(observer(FacilitiesListPanel)))