import styled from 'styled-components'

export const ListWrapper = styled.ul`
  li {
    list-style: none;
    font-size: 14px;
    border-bottom: 1px dashed #cdcdcd;
    padding: 10px 15px 10px 25px;
    position: relative;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    color: #333333;
    &:before {
      display: block;
      content: '\f0da';
      font-family: FontAwesome, sans-serif;
      position: absolute;
      left: 10px;
      top: 50%;
      transform: translateY(-50%);
      color: #ff7c00;
    }
    &:nth-child(odd) {
      background-color: #f2f2f2;
    }
    &:last-child {
      text-align: right;
      color: #517f3f;
      padding: 5px 10px;
      &:before {
        display: none;
      }
      .fa {
        margin-left: 5px;
      }
    }
  }
`