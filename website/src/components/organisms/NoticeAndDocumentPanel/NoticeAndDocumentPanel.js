import React, { Fragment } from 'react'
import BlockHeading from '../../elements/BlockHeading'
import { inject, observer } from 'mobx-react'
import { ListWrapper } from './styled-components'
import { withRouter, Link } from 'react-router-dom'

const NoticeAndDocumentPanel = props => {

  const {
    history,
    documentsStore, commonStore,
  } = props

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  return (
    <Fragment>
      <BlockHeading theme={'green'}>
        <i className="fa fa-file-text" aria-hidden="true"/>
        Văn Bản & Thông Báo
      </BlockHeading>
      <ListWrapper>
        {
          documentsStore.documentsList.length > 0
            ? (
              documentsStore.documentsList
                .slice(0, 5)
                .map(item =>
                  <li key={item.id}>
                    {item.title}
                  </li>,
                )
            ) : null
        }
        <li>
          Xem thêm
          <i className="fa fa-angle-double-right" aria-hidden="true"/>
        </li>
      </ListWrapper>
    </Fragment>
  )
}

export default withRouter(inject(
  'documentsStore', 'commonStore',
)(observer(NoticeAndDocumentPanel)))