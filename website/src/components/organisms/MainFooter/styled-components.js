import styled from 'styled-components'

export const FooterWrapper = styled.footer`
  padding: 20px 0;
  background: url("./footer-bg.png") #517f41 no-repeat center bottom;
`
export const FooterContentLeft = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  p {
    color: white;
    font-size: 14px;
    padding-left: 10px;
    line-height: 1.6;
    margin-bottom: 0;
  }
  img {
    box-shadow: 0 0 10px rgba(0,0,0,.5);
  }
`
export const FooterContentRight = styled.div`
  p {
    color: white;
    font-size: 14px;
    padding-left: 10px;
    line-height: 1.6;
    text-align: right;
  }
`