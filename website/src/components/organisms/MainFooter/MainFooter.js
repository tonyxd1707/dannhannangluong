import React from 'react'
import { FooterWrapper } from './styled-components'
import { FlexContainer } from '../../layout/Container/Container'
import {
  FooterContentLeft, FooterContentRight,
} from './styled-components'
import { withRouter, Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'

const MainFooter = props => {

  const { commonStore, history } = props

  const handleNavigate = (url, pageName) => {
    commonStore.setPage(pageName)
    history.push(url)
  }

  return (
    <FooterWrapper>
      <FlexContainer alignItems={'center'} justify={'space-between'}>
        <FooterContentLeft>
          <Link to={'/'} onClick={() => handleNavigate('/', 'Trang Chủ')}>
            <img src="./footer-logo.png" alt=""/>
          </Link>
          <p>
            Cơ quan chủ quản: DANNHANNANGLUONG.COM<br/>
            Giấy phép: 1102/GP-BTTTT Ngày cấp: 21/6/2012<br/>
            Tổng biên tập: Hoàng Thị Nghĩa
          </p>
        </FooterContentLeft>
        <FooterContentRight>
          <p>
            &copy; Copyright 2014 <strong>dannhannangluong.com</strong>, All right reserved<br/>
            &copy; <strong>dannhannangluong.com</strong> giữ bản quyền nội dung trên website này
          </p>
        </FooterContentRight>
      </FlexContainer>
    </FooterWrapper>
  )
}

export default withRouter(inject('commonStore')(observer(MainFooter)))