import styled from 'styled-components'

export const HeaderWrapper = styled.header`
  display: block;
  padding: 0 15px;
  position: relative;
  background: url("./city-head-bg.png") white no-repeat center bottom 50px;
  background-size: auto 60%;
`

export const NavigationWrapper = styled.nav`
  max-width: 1030px;
  margin-left: auto;
  margin-right: auto;
  display: block;
  ul {
    display: flex;
    align-items: center;
    padding: 10px 10px 0; 
    border-top-right-radius: 5px;
    border-top-left-radius: 5px;
    margin-bottom: 0;
    li {
      color: white;
      font-size: 14px;
      padding: 10px 15px;
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
      font-weight: 500;
      text-align: center;
      &:hover, &.current {
        cursor: pointer;
        background-color: #ffb200;
        color: #387034;
      }
    }
  }
`