import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { HeaderWrapper, NavigationWrapper } from './styled-components'
import { inject, observer } from 'mobx-react'
import { Container } from '../../layout/Container/Container'

const MainHeader = props => {

  const {
    history,
    commonStore,
  } = props

  const navigationList = [
    {
      title: 'Trang Chủ',
      url: '/',
    },
    {
      title: 'Giới Thiệu',
      url: '/gioi-thieu',
    },
    {
      title: 'Dịch Vụ Luật Sư',
      url: '/dich-vu/tat-ca',
    },
    {
      title: 'Liên Hệ',
      url: '/lien-he',
    },
    {
      title: 'Hỏi Đáp',
      url: '/hoi-dap',
    },
    {
      title: 'Đối Tác',
      url: '/doi-tac',
    },
  ]

  const handleNavigation = (url, pageName) => {
    history.push(url)
    commonStore.setPage(pageName)
  }

  return (
    <HeaderWrapper>
      <Container>
        <Link to='/' onClick={() => handleNavigation('/', 'Trang Chủ')}>
          <img src="/header-logo.png" alt=""/>
        </Link>
      </Container>
      <NavigationWrapper role="navigation">
        <ul className={'green-gradient'}>
          {
            navigationList.map((item, index) => {
              return (
                <li
                  className={commonStore.currentPage === item.title ? 'current' : null}
                  onClick={() => handleNavigation(item.url, item.title)}
                  key={index}>
                  {item.title}
                </li>
              )
            })
          }
        </ul>
      </NavigationWrapper>
    </HeaderWrapper>
  )
}

export default withRouter(inject('commonStore')(observer(MainHeader)))
