import React from 'react'
// Styling
import './App.less'
import './AntdOverride.less'
// React router
import { Router, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
// MobX
import { Provider } from 'mobx-react'
import loadingAnimationStore from './stores/loadingAnimationStore'
import commonStore from './stores/commonStore'
import articlesStore from './stores/articlesStore'
import documentsStore from './stores/documentsStore'
import facilitiesStore from './stores/facilitiesStore'
import partnersStore from './stores/partnersStore'
import questionsAnswersStore from './stores/questionsAnswersStore'
import standardsStore from './stores/standardsStore'
// Pages
import NotFoundPage from './components/pages/NotFoundPage'
import HomePage from './components/pages/HomePage'
import ContactPage from './components/pages/ContactPage'
import QAPage from './components/pages/QAPage'
import PartnersPage from './components/pages/PartnersPage'
import IntroPage from './components/pages/IntroPage'
import ArticlesPage from './components/pages/ArticlesPage'

const stores = {
  articlesStore,
  commonStore,
  documentsStore,
  facilitiesStore,
  partnersStore,
  questionsAnswersStore,
  standardsStore,
  loadingAnimationStore,
}

const history = createBrowserHistory()

const App = () => {
  return (
    <Provider {...stores}>
      <Router history={history}>
        <Switch>
          <Route exact path={'/'} component={HomePage}/>
          <Route exact path={'/lien-he'} component={ContactPage}/>
          <Route exact path={'/hoi-dap'} component={QAPage}/>
          <Route exact path={'/doi-tac'} component={PartnersPage}/>
          <Route exact path={'/gioi-thieu'} component={IntroPage}/>
          <Route exact path={'/dich-vu/:category'} component={ArticlesPage}/>
          <Route component={NotFoundPage}/>
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
