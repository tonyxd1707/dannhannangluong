import { observable, action, toJS } from 'mobx'
// Request
import { FacilityRequest } from '../requests'

class FacilitiesStore {

  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Article */
  @observable facilitiesList = []
  @observable facilityDetail = {}

  @action clearFacilitiesList() {
    this.facilitiesList = []
  }

  @action clearFacilityDetail() {
    this.facilityDetail = {}
  }

  @action getAllFacilities() {
    return new Promise((resolve, reject) => {
      FacilityRequest.getAllFacilities()
        .then(response => {
          this.facilitiesList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getFacilityById(facilityId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearFacilitiesList()
    this.clearFacilityDetail()
  }

}

export default new FacilitiesStore()