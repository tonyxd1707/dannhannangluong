import { observable, action, toJS } from 'mobx'
import { PartnerRequest } from '../requests'

// Request

class PartnersStore {

  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Article */
  @observable partnersList = []
  @observable partnerDetail = {}

  @action clearPartnersList() {
    this.partnersList = []
  }

  @action clearPartnerDetail() {
    this.partnerDetail = {}
  }

  @action getAllPartners() {
    return new Promise((resolve, reject) => {
      PartnerRequest.getAllPartners()
        .then(response => {
          this.partnersList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getPartnerById(partnerId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearPartnersList()
    this.clearPartnerDetail()
  }

}

export default new PartnersStore()