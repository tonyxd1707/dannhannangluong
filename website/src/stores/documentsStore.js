import { observable, action, toJS } from 'mobx'
// Request
import { DocumentRequest } from '../requests'

class DocumentsStore {

  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Document */
  @observable documentsList = []
  @observable documentDetail = {}

  @action clearDocumentsList() {
    this.documentsList = []
  }

  @action clearDocumentDetail() {
    this.documentDetail = {}
  }

  @action getAllDocuments() {
    return new Promise((resolve, reject) => {
      DocumentRequest.getAllDocuments()
        .then(response => {
          this.documentsList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getDocumentById(documentId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearDocumentsList()
    this.clearDocumentDetail()
  }

}

export default new DocumentsStore()