import { observable, action } from 'mobx'

class LoadingAnimationStore {

  /** Loading spinner state */
  @observable isVisible = false

  @action showSpinner(state) {
    this.isVisible = state
  }

}

export default new LoadingAnimationStore()