import { observable, action, toJS } from 'mobx'
// Request
import { ArticleRequest } from '../requests'

class ArticlesStore {
  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Article */
  @observable articlesList = []
  @observable articleDetail = {}

  @action clearArticlesList() {
    this.articlesList = []
  }

  @action clearArticleDetail() {
    this.articleDetail = {}
  }

  @action getAllArticles(params) {
    return new Promise((resolve, reject) => {
      ArticleRequest.getAllArticles(params)
        .then(response => {
          this.articlesList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getArticleById(articleId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearArticlesList()
    this.clearArticleDetail()
  }
}

export default new ArticlesStore()
