import { observable, action } from 'mobx'
// Request
import { QuestionRequest } from '../requests'

class QuestionsAnswersStore {

  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Article */
  @observable qaList = []
  @observable qaDetail = {}

  @action clearQAList() {
    this.qaList = []
  }

  @action clearQADetail() {
    this.qaDetail = {}
  }

  @action getAllQuestions() {
    return new Promise((resolve, reject) => {
      QuestionRequest.getAllQuestions()
        .then(response => {
          this.qaList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getQAById(qaId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearQAList()
    this.clearQADetail()
  }

}

export default new QuestionsAnswersStore()