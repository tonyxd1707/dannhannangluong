import { observable, action } from 'mobx'

class CommonStore {

  /** Token */
  @observable token = localStorage.getItem('token')

  @action getToken(token) {
    this.token = token
  }

  /** Current page - Shoule be a string */
  @observable currentPage = 'Trang Chủ'

  @action setPage(pageName) {
    this.currentPage = pageName
  }

  /** Locale */
  @observable defaultLocale = 'vi'

  @action setLocale(locale) {
    this.defaultLocale = locale
  }

  /** App theme */
  @observable appTheme = {
    name: 'default',
    solidColor: '#3DBEA3',
    solidLightColor: '#ecf9f6',
    gradientColor: 'linear-gradient(167.51deg, #2ECF94 24.37%, #3DBEA3 78.07%)',
    shadowColor: '0 2px 10px rgba(46,207,148,0.6)',
    lightShadowColor: '0 2px 4px rgba(61, 190, 163, 0.24), 0 4px 8px rgba(61, 190, 163, 0.16)',
  }

  @action setTheme(themeName) {
    switch (themeName) {
      case 'pink':
        this.appTheme.name = 'pink'
        this.appTheme.solidColor = 'rgb(244, 67, 54)'
        this.appTheme.solidLightColor = 'rgb(254, 237, 235)'
        this.appTheme.gradientColor = 'linear-gradient(108.84deg, #F77062 0%, #FE5196 100%)'
        this.appTheme.shadowColor = '0 2px 10px rgba(254, 81, 150, 0.5)'
        this.appTheme.lightShadowColor = '0 2px 4px rgba(190, 61, 97, 0.24), 0 4px 8px rgba(190, 61, 61, 0.16)'
        break
      case 'blue':
        this.appTheme.name = 'blue'
        this.appTheme.solidColor = 'rgb(33, 150, 243)'
        this.appTheme.solidLightColor = 'rgb(233, 245, 254)'
        this.appTheme.gradientColor = 'linear-gradient(108.88deg, #04BEFE 0%, #4481EB 100%)'
        this.appTheme.shadowColor = '0 2px 10px rgba(68, 129, 235, 0.5)'
        this.appTheme.lightShadowColor = '0 2px 4px rgba(61, 147, 190, 0.24), 0 4px 8px rgba(61, 153, 190, 0.16)'
        break
      case 'csstricks':
        this.appTheme.name = 'csstricks'
        this.appTheme.solidColor = 'rgb(229,46,113)'
        this.appTheme.solidLightColor = 'rgba(229,46,113, .2)'
        this.appTheme.gradientColor = 'linear-gradient(to top left,#ff8a00,#e52e71)'
        this.appTheme.shadowColor = '0px 2px 10px rgba(229,46,113, 0.5)'
        this.appTheme.lightShadowColor = '0 2px 4px rgba(190, 61, 97, 0.24), 0 4px 8px rgba(190, 61, 61, 0.16)'
        break
      case 'black':
        this.appTheme.name = 'black'
        this.appTheme.solidColor = 'rgb(70, 70, 70)'
        this.appTheme.solidLightColor = 'rgb(200, 200, 200)'
        this.appTheme.gradientColor = 'linear-gradient(108.88deg, rgb(121, 121, 121) 0%, rgb(70, 70, 70) 100%)'
        this.appTheme.shadowColor = '0px 2px 10px rgba(70, 70, 70, 0.5)'
        this.appTheme.lightShadowColor = '0 2px 4px rgba(0, 0, 0, 0.22), 0 4px 8px rgba(0, 0, 0, 0.04)'
        break
      default:
        break
    }
  }

}

export default new CommonStore()