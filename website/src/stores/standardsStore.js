import { observable, action, toJS } from 'mobx'
// Request
import { StandardRequest } from '../requests'

class StandardsStore {

  /** Loading state */
  @observable isLoading = false

  @action setLoadingProgress(state) {
    this.isLoading = state
  }

  /** Article */
  @observable standardsList = []
  @observable standardDetail = {}

  @action clearStandardsList() {
    this.standardsList = []
  }

  @action clearStandardDetail() {
    this.standardDetail = {}
  }

  @action getAllStandards() {
    return new Promise((resolve, reject) => {
      StandardRequest.getAllStandards()
        .then(response => {
          this.standardsList = response.data
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  @action getStandardById(standardId) {
    // Do request here
  }

  /** Clear store data */
  @action clearStore() {
    this.clearStandardsList()
    this.clearStandardDetail()
  }

}

export default new StandardsStore()