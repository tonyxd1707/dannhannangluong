import axios from 'axios'
import commonStore from './stores/commonStore'
import { apiUrl } from './config'

const requests = {
  get: (url, header = false) => {
    if (header) {
      return axios({
        method: `get`,
        url: `${apiUrl}${url}`,
        timeout: 20000,
        headers: {
          Authorization: `Bearer ${commonStore.token}`,
        },
      })
    }
    return axios({
      method: `get`,
      timeout: 20000,
      url: `${apiUrl}${url}`,
    })
  },
  post: (url, body, header = false) => {
    if (header) {
      return axios({
        method: `post`,
        url: `${apiUrl}${url}`,
        headers: {
          Authorization: `Bearer ${commonStore.token}`,
        },
        data: body,
        timeout: 20000,
      })
    }
    return axios({
      method: `post`,
      url: `${apiUrl}${url}`,
      data: body,
      timeout: 20000,
    })
  },
  delete: (url, header = false) => {
    if (header) {
      return axios({
        method: `delete`,
        url: `${apiUrl}${url}`,
        headers: {
          Authorization: `Bearer ${commonStore.token}`,
        },
        timeout: 20000,
      })
    }
    return axios({
      method: `delete`,
      url: `${apiUrl}${url}`,
      timeout: 20000,
    })
  },
  put: (url, body, header = false) => {
    if (header) {
      return axios({
        method: `put`,
        url: `${apiUrl}${url}`,
        headers: {
          Authorization: `Bearer ${commonStore.token}`,
        },
        data: body,
        timeout: 20000,
      })
    }
    if (body) {
      return axios({
        method: `put`,
        url: `${apiUrl}${url}`,
        data: body,
        timeout: 20000,
      })
    }
    return axios({
      method: `put`,
      url: `${apiUrl}${url}`,
      timeout: 20000,
    })
  },
}

export const ArticleRequest = {
  getAllArticles: (params) => {
    if (params) {
      return requests.get(`/articles${params}`)
    }
    return requests.get(`/articles`)
  },

}

export const DocumentRequest = {
  getAllDocuments: () =>
    requests.get(`/documents`),
}

export const FacilityRequest = {
  getAllFacilities: () =>
    requests.get(`/facilities`),
}

export const PartnerRequest = {
  getAllPartners: () =>
    requests.get(`/partners`),
}

export const QuestionRequest = {
  getAllQuestions: () =>
    requests.get(`/qalists`),
}

export const StandardRequest = {
  getAllStandards: () =>
    requests.get(`/standards`),
}